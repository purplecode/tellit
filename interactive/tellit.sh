#!/bin/bash


RED='\033[0;31m'
GREEN='\033[0;32m'
BLUE='\033[0;34m'
PURPLE='\033[0;35m'
NC='\033[0m' # No Color


function priv_pub_key {
  read -p"Name of the file you want to encrypt (with extension): " FILE
  echo -e "${PURPLE} This is the public-private key handling with openssl. What do you want to do?${NC}\n 1 - Create a pair and cipher\n 2 - Choose a public key to cipher"
  read -p"OPTION >> " OPT
  if [ "$OPT" == "1" ]
    then
        #create key pair using rsa
        read -p"Name of the private key (without the extension): " KEY
        echo -e "${PURPLE}Creating a private key...${NC}"
        openssl genrsa -out "$KEY".pem 1024
        echo -e "${PURPLE}Creating a public key...${NC}"
        openssl rsa -in "$KEY".pem -pubout > "$KEY".pub

        #Encrypting
        echo -e "${PURPLE}Encrypting...${NC}"
        openssl rsautl -encrypt -pubin -inkey "$KEY".pub -ssl -in $FILE -out "$FILE"_enc
        echo -e "${BLUE}Done! it's on '$FILE'_enc ${NC}"

  elif [ "$OPT" == "2" ]
    then
      #loquesea
      read -p "Write the public key for encrypting (without extension): " CLA
      echo -e "${PURPLE}Encrypting...${NC}"
      openssl rsautl -encrypt -pubin -inkey "$CLA".pub -ssl -in $FILE -out "$FILE"_enc
      echo -e "${BLUE}Done! it's on $FILE _enc ${NC}"
  else
    echo -e "\n${RED}Invalid option, please retry${NC}\n"
  fi
}

function dec_ {
  #list all private keys
  echo -e "${PURPLE}This is the decryption function. ${NC} \nThis are the private keys available:\n"
  ls *.pem

  #decrypt
  read -p"Please write the file you want to decrypt (with extension): " FILEE
  read -p"Please write the private key file name (without extension): " PRIV
  openssl rsautl -decrypt -in $FILEE -out "$FILEE"_de -inkey "$PRIV".pem
}

function dmp_u {
  #starts daemon of check user
  ./check.sh &
}

#function upload_ipfs {
  #this is gonna be a pain right
  #did you started the daemon? remember to!
  #add or put?
  #ipfs add/put
#}

#function configure_ipf {
  #choose what to configure?
  #configure hash / change default values
  #done!
#}

#function configure_vpn {
  #adding vpn? later later~
#}

#help_menu()
dec_
